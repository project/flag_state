<?php
// $Id$
/**
 * @file flag_state.module
 *
 * A very simple implementation of a state flag type. The flag is useful
 * as a quick way of adding user/global switches that can be used as conditions
 * in rule evaluations. Sample use would be to enable/disable rule
 * based browsing history. The flag can be displayed on a block or elsewhere.
 * Either do this in your theme, custom module or use the php filter in a custom
 * block etc. The code to render the flag is:
 *
 * @code
 *  return flag_create_link(<flag_name>, 1);
 * @endcode
 */

/**
 * Implementation of hook_flag_definitions().
 *
 * Defines the flags this module implements.
 *
 * @return
 *   An "array of arrays", keyed by content-types. The 'handler' slot
 *   should point to the PHP class implementing this flag.
 */
function flag_state_flag_definitions() {
  return array(
    'flag_state' => array(
      'title' => t('Flag State'),
      'description' => t("Content is state - doesn't refer to anything else."),
      'handler' => 'flag_state',
    ),
  );
}

/**
 * Implements a state flag. Currently set or not set. The content is the state.
 */
class flag_state extends flag_flag {
  function options() {
    $options = parent::options();
    // add any options here
    $options += array(
      'flag_confirmation_desc' => '',
      'unflag_confirmation_desc' => '',
      );
    return $options;
  }

  function options_form(&$form) {
    parent::options_form($form);
    // add bespoke config options to $form here

    // remove the node type element - should not be there
    unset($form['access']['types']);

    $form['display']['link_options_confirm']['flag_confirmation_desc'] = array(
      '#type' => 'textarea',
      '#title' => t('Flag confirmation descriptive message'),
      '#default_value' => $this->flag_confirmation_desc,
      '#description' => t('Descriptive message displayed if the user has clicked the "flag this" link and confirmation is required.'),
      '#access' => empty($this->locked['flag_confirmation_desc']),
    );

    $form['display']['link_options_confirm']['unflag_confirmation_desc'] = array(
      '#type' => 'textarea',
      '#title' => t('Unflag confirmation descriptive message'),
      '#default_value' => $this->unflag_confirmation_desc,
      '#description' => t('Descriptive message displayed if the user has clicked the "unflag this" link and confirmation is required.'),
      '#access' => empty($this->locked['unflag_confirmation_desc']),
    );
  }

  function applies_to_content_object($content) {
    return TRUE;
  }

  function get_content_id($content) {
    return 1;
  }
}

/**
 * Implementation of hook_form_FORM_ID_alter for flag settings form - flag_form.
 */
function flag_state_form_flag_form_alter(&$form, &$form_state) {
  // if flag is type of flag state add link options - could do this in link_types_alter but no flag type awareness there.
  if ($form['#flag']->content_type == 'flag_state') {
    $form['display']['link_type']['#flag_link_fields'][] = 'flag_confirmation_desc';
    $form['display']['link_type']['#flag_link_fields'][] = 'unflag_confirmation_desc';
  }
}

/**
 * Implementation of hook_form_FORM_ID_alter for flag confirm form - flag_confirm.
 * Adds the confirmation description to the form.
 */
function flag_state_form_flag_confirm_alter(&$form, &$form_state) {
  $flag = flag_get_flag($form['flag_name']['#value']);
  $action = $form['action']['#value'];
  $content_id = $form['content_id']['#value'];
  $description = $flag->get_label($action .'_confirmation_desc', $content_id);

  $form['description'] = array('#value' => $description);
}

/**
 * Implementation of hook_rules_condition_info().
 */
function flag_state_rules_condition_info() {
  return array(
    'flag_state_flag_condition' => array(
      'label' => t('State flag set'),
      'module' => 'Flag State',
      'eval input' => array('flag_name'),
      ),
    );
}

/**
 * The Rules condition function to return the state of the named flag for the current user.
 */
function flag_state_flag_condition($settings) {
  $flagged_content = flag_get_user_flags('flag_state');
  if (!empty($flagged_content[$settings['flag_name']])) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
* The Rules condition configuration form to set the state flag to test.
*/
function flag_state_flag_condition_form($settings, &$form) {
  $settings += array('flag_name' => '');

  $flag_names = drupal_map_assoc(_flag_get_flag_names());

  // choose flag to control user settings
  $state_flags = flag_get_flags('flag_state');
  $state_flag_names[''] = t('None selected');
  foreach ($state_flags as $flag) {
    $state_flag_names[$flag->name] = $flag->name;
  }

  $form['settings']['flag_name'] = array(
    '#type' => 'select',
    '#title' => t('Select State Flag'),
    '#default_value' => $settings['flag_name'],
    '#required' => TRUE,
    '#description' => t('State flags can be used to set per-user and global conditions.'),
    '#options' => $state_flag_names,
  );
}
